<?php

namespace Drupal\merlinone;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Image\ImageFactory;
use Drupal\Core\StreamWrapper\StreamWrapperManagerInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\StringTranslation\TranslationInterface;

/**
 * MerlinOne API service.
 */
class MerlinOneApi implements MerlinOneApiInterface {

  use StringTranslationTrait;

  /**
   * The Config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  private $configFactory;

  /**
   * The file system interface.
   *
   * @var \Drupal\Core\File\FileSystemInterface
   */
  protected $fileSystem;

  /**
   * The stream wrapper manager.
   *
   * @var \Drupal\Core\StreamWrapper\StreamWrapperManagerInterface
   */
  protected $streamWrapperManager;

  /**
   * @var \Drupal\Core\Image\ImageFactory
   */
  private $imageFactory;

  /**
   * Archive URL.
   *
   * @var string
   */
  protected $url;

  /**
   * Current API version.
   *
   * @var string
   */
  private $apiVersion;

  /**
   * Constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   Config factory.
   * @param \Drupal\Core\StringTranslation\TranslationInterface $string_translation
   *   String translation manager.
   * @param \Drupal\Core\File\FileSystemInterface $file_system
   *   The file system interface.
   * @param \Drupal\Core\StreamWrapper\StreamWrapperManagerInterface $stream_wrapper_manager
   *   The steam wrapper manager.
   */
  public function __construct(ConfigFactoryInterface $config_factory, TranslationInterface $string_translation, FileSystemInterface $file_system, StreamWrapperManagerInterface $stream_wrapper_manager, ImageFactory $imageFactory) {
    $this->configFactory = $config_factory;
    $this->stringTranslation = $string_translation;
    $this->fileSystem = $file_system;
    $this->streamWrapperManager = $stream_wrapper_manager;
    $this->imageFactory = $imageFactory;
  }

  /**
   * {@inheritdoc}
   */
  public function setArchiveUrl($url) {
    if (substr($url, -1, 1) !== '/') {
      $url = $url . '/';
    }

    $this->url = $url;
  }

  /**
   * {@inheritdoc}
   */
  public function getArchiveUrl() {
    if (!$this->url) {
      $config = $this->configFactory->get('merlinone.settings');
      $this->setArchiveUrl($config->get('archive_url'));
    }

    return $this->url;
  }

  /**
   * {@inheritdoc}
   */
  public function getApiBaseUrl() {
    return $this->getArchiveUrl() . 'mXchange/v1/';
  }

  /**
   * {@inheritdoc}
   */
  public function getMxUrl() {
    return $this->getArchiveUrl() . '?disposition=CMS';
  }

  /**
   * {@inheritdoc}
   */
  public function createPlaceholderFromItem($item, $transaction, $settings, $directory) {
    $itemUrl = $item['thumbnails']['large'];
    $metadata = $item['metadata'];
    $filename = $this->getFilenameForItem($item);

    // If limiting file extensions, force thumbnail to JPEG.
    // Placeholders for documents not limiting extensions will be a JPEG from the thumbnail data but should have the extension of the final file.
    if ($settings['should_limit_extensions'] !== 'false') {
      $info = pathinfo($filename);
      $filename = basename($filename,'.' . $info['extension']) . '.jpg';
    }

    $this->fileSystem->prepareDirectory($directory, FileSystemInterface::CREATE_DIRECTORY);

    // Destination is the upload directory and the filename.
    $destination = $this->streamWrapperManager->normalizeUri($directory . '/' . $filename);

    /** @var \Drupal\file\FileInterface $file */
    $file = system_retrieve_file($itemUrl, $destination, true, FileSystemInterface::EXISTS_RENAME);

    // If source width and height are specified, or there's a crop, resize the
    // thumbnail to the target dimensions so the created Media entity will have
    // the correct information after finalization.
    $hasMetadataDimensions = !empty($metadata['pixels320']) && !empty($metadata['lines330']);
    $hasCrop = !empty($transaction['image.pixels']);

    if ($settings['should_limit_extensions'] !== 'false' && ($hasMetadataDimensions || $hasCrop)) {
      $width = $hasCrop ? $transaction['image.pixels'] : $metadata['pixels320'];
      $height = $hasCrop ? intval($metadata['lines330'] * $transaction['image.pixels'] / $metadata['pixels320']) : $metadata['lines330'];

      $image = $this->imageFactory->get($file->getFileUri());
      $image->resize($width, $height);
      $image->save();
    }

    return $file;
  }

  /**
   * {@inheritdoc}
   */
  public function getFilenameForItem($item) {
    // Metadata for item.
    $metadata = $item['metadata'];

    // Filename for item.
    $filename = substr(trim($metadata['cobject205']), 0, 64);

    // Hires filename for item, which determines the final file extension.
    $hires = trim($metadata['hires']);

    // Parse filenames.
    $filename_info = pathinfo($filename);
    $hires_info = pathinfo($hires);

    // Get base filename without extension.
    $filename_base = basename($filename,'.' . $filename_info['extension']);

    // Return the base filename with hires extension.
    return $filename_base . '.' . strtolower($hires_info['extension']);
  }

}
