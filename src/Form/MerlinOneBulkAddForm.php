<?php

namespace Drupal\merlinone\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\media\MediaTypeInterface;
use Drupal\merlinone\Plugin\media\Source\MerlinOneMediaSourceInterface;
use Drupal\merlinone\Traits\MerlinOneBrowserTrait;

/**
 * Bulk add form
 */
class MerlinOneBulkAddForm extends FormBase {

  use MerlinOneBrowserTrait;

  /**
   * Returns a unique string identifying the form.
   *
   * @return string
   *   The unique string identifying the form.
   */
  public function getFormId() {
    return 'merlinone_bulk_add_form';
  }

  /**
   * Form constructor.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   * @param \Drupal\media\MediaTypeInterface|null $media_type
   *   The media type configuration entity.
   *
   * @return array
   *   The form structure.
   *
   * @throws \Exception
   */
  public function buildForm(array $form, FormStateInterface $form_state, MediaTypeInterface $media_type = NULL) {
    if ($media_type === NULL || !$media_type->getSource() instanceof MerlinOneMediaSourceInterface) {
      return $form;
    }

    // Embedded search.
    $form['merlinone_search'] = $this->getMerlinBrowserEmbed(700);

    $form['#attached']['library'][] = 'merlinone/bulk-add';
    $form['#attached']['drupalSettings']['merlinone'] = $this->getMerlinBrowserSettings($media_type);

    // Hold the response from the search.
    $form['merlinone_items'] = [
      '#type' => 'hidden',
      '#default_value' => '',
    ];

    $form['media_type'] = [
      '#type' => 'value',
      '#value' => $media_type->id(),
    ];

    return $form;
  }

  /**
   * Submit handler to create the media entities.
   *
   * @param array $form
   *   The form render array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $form_state->setRedirect('entity.media.collection');
  }

}
