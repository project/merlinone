<?php

namespace Drupal\merlinone\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\File\FileSystemInterface;
use Drupal\image\Entity\ImageStyle;
use Drupal\media\Entity\Media;
use Drupal\merlinone\MerlinOneApiInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\File\MimeType\MimeTypeGuesserInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Contains methods for media finalization for MerlinOne embedded browsers.
 */
class MerlinOneFinalizeMediaController extends ControllerBase {

  /**
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The MerlinOne API Service.
   *
   * @var \Drupal\merlinone\MerlinOneApiInterface
   */
  protected $merlinOneApi;

  /**
   * The file system interface.
   *
   * @var \Drupal\Core\File\FileSystemInterface
   */
  protected $fileSystem;

  /**
   * The mime type guesser service.
   *
   * @var \Symfony\Component\HttpFoundation\File\MimeType\MimeTypeGuesserInterface
   */
  protected $mimeTypeGuesser;

  /**
   * Constructs widget plugin.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager service.
   * @param \Drupal\merlinone\MerlinOneApiInterface $merlinOneApi
   *   The MerlinOne API service.
   * @param \Drupal\Core\File\FileSystemInterface $fileSystem
   *   The file system interface.
   * @param \Symfony\Component\HttpFoundation\File\MimeType\MimeTypeGuesserInterface $mimeTypeGuesser
   *   The MIME type guesser.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, MerlinOneApiInterface $merlinOneApi, FileSystemInterface $fileSystem, MimeTypeGuesserInterface $mimeTypeGuesser) {
    $this->entityTypeManager = $entity_type_manager;
    $this->merlinOneApi = $merlinOneApi;
    $this->fileSystem = $fileSystem;
    $this->mimeTypeGuesser = $mimeTypeGuesser;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('merlinone.api'),
      $container->get('file_system'),
      $container->get('file.mime_type.guesser')
    );
  }

  /**
   * Returns the media type that this widget creates.
   *
   * @param string $media_type
   *   The media type ID.
   *
   * @return \Drupal\media\MediaTypeInterface
   *   Media type.
   */
  protected function getType($media_type) {
    return $this->entityTypeManager
      ->getStorage('media_type')
      ->load($media_type);
  }

  /**
   * Finalize Media entities by replacing their source file with the full asset
   * URL.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The current request.
   * @param $media_type
   *   The media type.
   * @param $media_id
   *   The media entity ID.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   */
  public function finalizeMedia(Request $request, $media_type, $media_id) {
    $type = $this->getType($media_type);
    $plugin = $type->getSource();
    $sourceFieldDefinition = $plugin->getSourceFieldDefinition($type);

    // Load the media item and get its managed file from the source field.
    /** @var \Drupal\media\MediaInterface $mediaItem */
    $mediaItem = Media::load($media_id);
    $sourceField = $mediaItem->get($sourceFieldDefinition->getName());

    /** @var \Drupal\file\FileInterface $mediaFile */
    $mediaFile = $sourceField->entity;
    $mediaFileUri = $mediaFile->getFileUri();

    // Replace the file on disk with the data from the item's fullAssetUrl.
    $item = $request->get('item');
    $transaction = $request->get('transaction');
    system_retrieve_file($item['fullAssetUrl'], $mediaFileUri, FALSE, FileSystemInterface::EXISTS_REPLACE);

    // Get path, filename, and extensions.
    $mediaFilePathInfo = pathinfo($mediaFileUri);
    $filenameWithoutExtension = basename($mediaFileUri,'.' . $mediaFilePathInfo['extension']);

    $originalFilename = $this->merlinOneApi->getFilenameForItem($item);
    $originalFilenamePathInfo = pathinfo($originalFilename);
    $originalFilenameWithoutExtension = basename($originalFilename,'.' . $originalFilenamePathInfo['extension']);

    // Move the uploaded thumbnail file so the extension matches the final
    // file type.
    if (!empty($transaction['image.format'])) {
      // There's an image conversion in the transaction, so replace the thumb's
      // file extension.
      $format = $transaction['image.format'];
      $targetFilename = $filenameWithoutExtension . '.' . strtolower($format);
      $targetMerlinFilename = $originalFilenameWithoutExtension . '.' . strtolower($format);
    }
    else {
      // There's no image conversion, so replace the thumb's file extension
      // with the original filename's extension.
      $targetFilename = $originalFilenameWithoutExtension . '.' . strtolower($originalFilenamePathInfo['extension']);
      $targetMerlinFilename = $targetFilename;
    }

    $targetPath = $mediaFilePathInfo['dirname'] . DIRECTORY_SEPARATOR . $targetFilename;

    // Move the file if the target filename is different.
    if ($targetFilename !== basename($mediaFileUri)) {
      $mediaFileUri = $this->fileSystem->move($mediaFileUri, $targetPath);
    }

    // Update the File entity.
    $size = @filesize($mediaFileUri);
    $mediaFile->setFilename($targetFilename);
    $mediaFile->setFileUri($mediaFileUri);
    $mediaFile->setMimeType($this->mimeTypeGuesser->guess($targetFilename));
    $mediaFile->setSize($size);
    $mediaFile->save();

    // Update the Media filename
    $mediaItem->setName($targetMerlinFilename);
    $mediaItem->setNewRevision(FALSE);
    $mediaItem->save();

    // Flush all image styles.
    $styles = ImageStyle::loadMultiple();
    /** @var ImageStyle $style */
    foreach ($styles as $style) {
      $style->flush($mediaFileUri);
    }

    return new JsonResponse(['message' => 'Media finalized']);
  }

}
