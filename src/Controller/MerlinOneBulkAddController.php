<?php

namespace Drupal\merlinone\Controller;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Link;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Url;
use Drupal\media\MediaTypeInterface;
use Drupal\merlinone\Plugin\media\Source\MerlinOneMediaSourceInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Bulk add controller
 */
class MerlinOneBulkAddController extends ControllerBase {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Constructs a new MediaBulkUploadController.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager) {
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager')
    );
  }

  /**
   * Displays add links for the available bundles.
   *
   * Redirects to the add form if there's only one bundle available.
   *
   * @return \Symfony\Component\HttpFoundation\RedirectResponse|array
   *   If there's only one available bundle, a redirect response.
   *   Otherwise, a render array with the add links for each bundle.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   */
  public function addLinksList() {
    $build = [
      '#theme' => 'entity_add_list',
      '#bundles' => [],
    ];

    $entity_type = $this->entityTypeManager->getDefinition('media_type');
    $build['#cache']['tags'] = $entity_type->getListCacheTags();

    $build['#add_bundle_message'] = $this->t('No MerlinOne media types found.');

    $merlinTypes = $this->getMerlinMediaTypes();

    $form_route_name = 'merlinone.bulk.upload_form';
    if (count($merlinTypes) == 1) {
      $mediaType = reset($merlinTypes);
      return $this->redirect($form_route_name, ['media_type' => $mediaType->id()]);
    }

    foreach ($merlinTypes as $merlinType) {
      $link = Link::createFromRoute($merlinType->label(), $form_route_name, ['media_type' => $merlinType->id()]);
      if (!$link->getUrl()->access()) {
        continue;
      }

      $build['#bundles'][$merlinType->id()] = [
        'label' => $merlinType->label(),
        'description' => '',
        'add_link' => Link::createFromRoute($merlinType->label(), $form_route_name, ['media_type' => $merlinType->id()]),
      ];
    }

    return $build;
  }

  /**
   * Access callback to validate if the user has access to the upload form list.
   *
   * @param \Drupal\Core\Session\AccountInterface $account
   *   User to validate access on.
   *
   * @return \Drupal\Core\Access\AccessResultAllowed|\Drupal\Core\Access\AccessResultForbidden
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   */
  public function accessList(AccountInterface $account) {
    if ($account->hasPermission('administer media')) {
      return AccessResult::allowed();
    }

    $merlinTypes = $this->getMerlinMediaTypes();

    foreach ($merlinTypes as $merlinType) {
      $url = Url::fromRoute('merlinone.bulk.upload_form', ['media_type' => $merlinType->id()]);
      if ($url->access()) {
        return AccessResult::allowed();
      }
    }

    return AccessResult::forbidden('No MerlinOne media types accessible for the user.');
  }

  /**
   * Access callback to validate if the user has access to a bulk upload form.
   *
   * @param \Drupal\Core\Session\AccountInterface $account
   *   User to validate access on.
   * @param \Drupal\media\MediaTypeInterface $media_type
   *   The media type the upload form belongs to.
   *
   * @return \Drupal\Core\Access\AccessResultInterface
   */
  public function accessForm(AccountInterface $account, MediaTypeInterface $media_type) {
    $permissions = [
      'administer media',
      'create media',
      'create ' . $media_type->label() . ' media',
    ];
    return AccessResult::allowedIfHasPermissions($account, $permissions, 'OR');
  }

  /**
   * The _title_callback for the merlinone.bulk.upload_form route.
   *
   * @param \Drupal\media\MediaTypeInterface $media_type
   *   The current media type.
   *
   * @return string
   *   The page title.
   */
  public function bulkUploadTitle(MediaTypeInterface $media_type) {
    return $this->t('Bulk add @name', ['@name' => $media_type->label()]);
  }

  /**
   * Finds all of the media types that have a MerlinOne source type.
   *
   * @return array
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  protected function getMerlinMediaTypes() {
    $mediaStorage = $this->entityTypeManager->getStorage('media_type');
    $mediaTypes = $mediaStorage->loadMultiple();

    // Only display types with MerlinOne sources
    $merlinTypes = [];
    foreach ($mediaTypes as $type) {
      if ($type->getSource() instanceof MerlinOneMediaSourceInterface) {
        $field_map = $type->getFieldMap();
        if (!empty($field_map[MerlinOneMediaSourceInterface::MERLIN_ID_SOURCE_FIELD])) {
          $merlinTypes[] = $type;
        }
      }
    }

    return $merlinTypes;
  }
}
