<?php

namespace Drupal\merlinone\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Utility\Token;
use Drupal\media\Entity\Media;
use Drupal\merlinone\MerlinOneApiInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Contains methods for media preparation for MerlinOne embedded browsers.
 */
class MerlinOnePrepareMediaController extends ControllerBase {

  /**
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The MerlinOne API Service.
   *
   * @var \Drupal\merlinone\MerlinOneApiInterface
   */
  protected $merlinOneApi;

  /**
   * @var \Drupal\Core\Utility\Token
   */
  private $token;

  /**
   * Constructs widget plugin.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager service.
   * @param \Drupal\merlinone\MerlinOneApiInterface $merlinOneApi
   *   The MerlinOne API service.
   * @param \Drupal\Core\Utility\Token $token
   *   The token utility.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, MerlinOneApiInterface $merlinOneApi, Token $token) {
    $this->entityTypeManager = $entity_type_manager;
    $this->merlinOneApi = $merlinOneApi;
    $this->token = $token;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('merlinone.api'),
      $container->get('token')
    );
  }

  /**
   * Returns the media type that this widget creates.
   *
   * @param string $media_type
   *   The media type ID.
   *
   * @return \Drupal\media\MediaTypeInterface
   *   Media type.
   */
  protected function getType($media_type) {
    return $this->entityTypeManager
      ->getStorage('media_type')
      ->load($media_type);
  }

  /**
   * Prepare Media entities by using their large thumbnail.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The current request.
   * @param string $media_type
   *   The media type to create.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   */
  public function prepareMedia(Request $request, $media_type) {
    $type = $this->getType($media_type);
    $plugin = $type->getSource();
    $sourceField = $plugin->getSourceFieldDefinition($type);
    $uploadDir = $this->getUploadLocation($sourceField);

    $item = $request->get('item');
    $transaction = $request->get('transaction');
    $settings = $request->get('settings');
    $file = $this->merlinOneApi->createPlaceholderFromItem($item, $transaction, $settings, $uploadDir);

    if ($file) {
      $file->setOwnerId($this->currentUser()->id());
      $file->save();

      $mediaItem = Media::create([
        'bundle' => $type->id(),
        $sourceField->getName() => $file->id(),
        'name' => !empty($item['headline']) ? $item['headline'] : NULL,
        // Keep original item to use when filling in mapped media fields.
        'original_item' => $item,
      ]);
      $mediaItem->save();

      // Set the entity ID in the original item and pass it back to JS.
      $item['entityId'] = $mediaItem->id();

      return new JsonResponse($item);
    }

    return new JsonResponse(['message' => 'Error creating Media entity'], 500);
  }

  /**
   * Gets upload location.
   *
   * @param $sourceField
   *
   * @return string
   *   Destination folder URI.
   */
  protected function getUploadLocation(FieldDefinitionInterface $sourceField) {
    $replaced = $this->token->replace($sourceField->getSetting('file_directory'));
    return $sourceField->getSetting('uri_scheme') . '://' . $replaced;
  }

}
