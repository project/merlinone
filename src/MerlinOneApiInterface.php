<?php

namespace Drupal\merlinone;

/**
 * Provides the MerlinOne API.
 */
interface MerlinOneApiInterface {

  /**
   * Sets the Archive URL.
   *
   * @param string $url
   *   The archive base URL.
   */
  public function setArchiveUrl($url);

  /**
   * Gets the Archive URL.
   *
   * @return string
   *   The archive base URL.
   */
  public function getArchiveUrl();

  /**
   * Gets the API base URL.
   *
   * @return string
   *   The API base URL.
   */
  public function getApiBaseUrl();

  /**
   * Gets the MX embed URL.
   *
   * @return string
   *   The MX embed URL.
   */
  public function getMxUrl();

  /**
   * Creates placeholder file from a Merlin item and transaction.
   *
   * @param mixed $item
   *   Item information from the Merlin search.
   * @param mixed $transaction
   *   Transaction information from the Merlin search.
   * @param mixed $settings
   *   Settings for the current search.
   * @param string $directory
   *   Destination directory.
   *
   * @return \Drupal\file\FileInterface
   *   A managed file
   */
  public function createPlaceholderFromItem($item, $transaction, $settings, $directory);

  /**
   * Get the item's filename.
   *
   * @param mixed $item
   *   Item information from the Merlin search.
   *
   * @return string
   *   The item's filename.
   */
  public function getFilenameForItem($item);

}
