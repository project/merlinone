<?php

namespace Drupal\merlinone\Plugin\EntityBrowser\Widget;

use Drupal\Component\Serialization\Json;
use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Link;
use Drupal\entity_browser\WidgetBase;
use Drupal\media\Entity\Media;
use Drupal\media\MediaTypeInterface;
use Drupal\merlinone\Plugin\media\Source\MerlinOneMediaSourceInterface;
use Drupal\merlinone\Traits\MerlinOneBrowserTrait;

/**
 * An Entity Browser widget to create media from the MerlinOne library.
 *
 * @EntityBrowserWidget(
 *   id = "merlinone_search",
 *   label = @Translation("MerlinOne search"),
 *   description = @Translation("Search and import from the MerlinOne library")
 * )
 */
class MerlinOneSearch extends WidgetBase {

  use MerlinOneBrowserTrait;

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'media_type' => NULL,
      'search_height' => 410,
    ] + parent::defaultConfiguration();
  }

  /**
   * Returns the media type that this widget creates.
   *
   * @return \Drupal\media\MediaTypeInterface
   *   Media type.
   */
  protected function getType() {
    return $this->entityTypeManager
      ->getStorage('media_type')
      ->load($this->configuration['media_type']);
  }

  /**
   * {@inheritdoc}
   */
  public function getForm(array &$original_form, FormStateInterface $form_state, array $additional_widget_parameters) {
    $form = parent::getForm($original_form, $form_state, $additional_widget_parameters);

    if (!$this->typeHasMerlinIdMapped($this->getType())) {
      $id_message = $this->t('The <em>Merlin ID</em> field must be mapped before importing assets from MerlinOne');

      $form['merlinone_search'] = [
        '#type' => 'markup',
        '#markup' => '<p class="color-error">' . $id_message . '</p>',
      ];

      return $form;
    }

    $browser_height = 410;
    if (!empty($this->configuration['search_height']) && is_numeric($this->configuration['search_height']) && $this->configuration['search_height'] > 0) {
      $browser_height = $this->configuration['search_height'];
    }

    // Embedded search.
    $form['merlinone_search'] = $this->getMerlinBrowserEmbed($browser_height);

    // Attach library and pass domain settings.
    $form['#attached']['library'][] = 'merlinone/entity-browser';
    $form['#attached']['drupalSettings']['merlinone'] = $this->getMerlinBrowserSettings($this->getType());

    // Hold the response from the search.
    $form['merlinone_items'] = [
      '#type' => 'hidden',
      '#default_value' => '',
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    $configuration = $this->configuration;

    $form['search_height'] = [
      '#type' => 'number',
      '#title' => $this->t('MerlinOne Search Height'),
      '#default_value' => $configuration['search_height'],
    ];

    $form['media_type'] = [
      '#type' => 'select',
      '#title' => $this->t('Media type'),
      '#required' => TRUE,
      '#description' => $this->t('The type of media entity to create from the uploaded file(s).'),
      '#element_validate' => [[$this, 'validateType']],
    ];

    $type = $this->getType();
    if ($type) {
      $form['media_type']['#default_value'] = $type->id();
    }

    // Find media types that implement MerlinOneMediaSourceInterface.
    $types = [];
    foreach ($this->entityTypeManager->getStorage('media_type')->loadMultiple() as $type) {
      if ($type->getSource() instanceof MerlinOneMediaSourceInterface) {
        $types[] = $type;
      }
    }

    if (!empty($types)) {
      foreach ($types as $type) {
        $form['media_type']['#options'][$type->id()] = $type->label();
      }
    }
    else {
      $form['media_type']['#disabled'] = TRUE;
      $form['media_type']['#description'] = $this->t('You must @create_type before using this widget.', [
        '@create_type' => Link::createFromRoute($this->t('create a media type'), 'entity.media_type.add_form')->toString(),
      ]);
    }

    return $form;
  }

  /**
   * Validate Media Type
   *
   * @param $element
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   * @param $form
   */
  public function validateType($element, FormStateInterface $form_state, $form) {
    $type_id = $form_state->getValue($element['#parents']);

    if ($type_id) {
      /** @var \Drupal\media\MediaTypeInterface $type */
      $type = $this->entityTypeManager
        ->getStorage('media_type')
        ->load($type_id);

      if (!$this->typeHasMerlinIdMapped($type)) {
        $id_message = $this->t('The <em>Merlin ID</em> field must be mapped before importing assets from MerlinOne');
        $form_state->setError($element, $id_message);
      }
    }
  }

  /**
   * Indicates if the Media Type source has the Merlin ID field mapped.
   *
   * @param \Drupal\media\MediaTypeInterface $type
   * @return bool
   */
  protected function typeHasMerlinIdMapped(MediaTypeInterface $type) {
    $source = $type->getSource();

    if (!$source instanceof MerlinOneMediaSourceInterface) {
      return FALSE;
    }

    $field_map = $type->getFieldMap();
    return !empty($field_map[MerlinOneMediaSourceInterface::MERLIN_ID_SOURCE_FIELD]);
  }

  /**
   * {@inheritdoc}
   */
  public function calculateDependencies() {
    $dependencies = parent::calculateDependencies();

    // Depend on the media type this widget creates.
    $type = $this->getType();
    $dependencies[$type->getConfigDependencyKey()][] = $type->getConfigDependencyName();
    $dependencies['module'][] = 'media';

    return $dependencies;
  }

  /**
   * {@inheritdoc}
   */
  public function prepareEntities(array $form, FormStateInterface $form_state) {
    $media = [];

    if ($merlinone_items = $form_state->getValue('merlinone_items')) {
      $items = Json::decode($merlinone_items);

      foreach ($items as $item) {
        $media[] = Media::load($item['entityId']);
      }
    }

    // Keep reference to the prepared entities.
    $form_state->set('merlinone_prepared_entities', $media);

    return $media;
  }

  /**
   * {@inheritdoc}
   */
  public function submit(array &$element, array &$form, FormStateInterface $form_state) {
    /** @var \Drupal\media\MediaInterface[] $media */
    $media = $form_state->get('merlinone_prepared_entities');
    $this->selectEntities($media, $form_state);
    $this->clearFormValues($element, $form_state);
  }

  /**
   * Clear values from Merlin response form element.
   *
   * @param array $element
   *   Upload form element.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Form state object.
   */
  protected function clearFormValues(array &$element, FormStateInterface $form_state) {
    // The hidden value and user input needs to be removed otherwise files will
    // re-download when the selection form is submitted.
    $form_state->setValueForElement($element['merlinone_items'], '');
    NestedArray::setValue($form_state->getUserInput(), $element['merlinone_items']['#parents'], '');
  }

}
