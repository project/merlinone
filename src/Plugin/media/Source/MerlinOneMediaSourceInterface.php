<?php
namespace Drupal\merlinone\Plugin\media\Source;

use Drupal\media\MediaTypeInterface;

/**
 * Defines the interface for MerlinOne media source plugins.
 */
interface MerlinOneMediaSourceInterface {

  const MERLIN_ID_SOURCE_FIELD = 'merlin_id';

  /**
   * Get supported file extensions for the given media type.
   *
   * @param \Drupal\media\MediaTypeInterface $media_type
   *   The media type to check.
   * @return array
   *   An array of file extensions.
   */
  public function getSupportedExtensions(MediaTypeInterface $media_type);

  /**
   * Indicates if the file extensions should be limited for the given media
   * type (currently need to force JPEG for non-supported, so only image media
   * types should limit).
   *
   * @param \Drupal\media\MediaTypeInterface $media_type
   *   The media type to check.
   * @return bool
   */
  public function shouldLimitExtensions(MediaTypeInterface $media_type);

}
