<?php

namespace Drupal\merlinone\Traits;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Form\SubformStateInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\media\MediaInterface;

/**
 * A MerlinOne Media Source.
 */
trait MerlinOneMediaSourceTrait {

  use StringTranslationTrait;

  /**
   * Gets a list of metadata attributes provided by Merlin.
   *
   * @return array
   *   Associative array with:
   *   - keys: metadata attribute names
   *   - values: human-readable labels for those attribute names
   */
  protected function getMerlinMetadataAttributes() {
    return [
      'headline' => $this->t('Headline'),
      'caption' => $this->t('Caption'),
      'copyright' => $this->t('Copyright'),
      'merlin_id' => $this->t('Merlin ID'),
      'keywords' => $this->t('Keywords'),
    ];
  }

  /**
   * Handle Merlin metadata attribute for a given media item.
   *
   * @param \Drupal\media\MediaInterface $media
   *   A media item.
   * @param string $attribute_name
   *   Name of the attribute to fetch.
   *
   * @return mixed|null
   *   Metadata attribute value or NULL if unavailable.
   */
  protected function getMerlinMetadata(MediaInterface $media, $attribute_name) {
    $field_map = $media->bundle->entity->getFieldMap();

    // $original_item holds the response from Merlin after entity creation,
    // so pull data from there if it exists, otherwise get the values from the
    // media entity.
    if (isset($media->original_item['metadata']) && $metadata = $media->original_item['metadata']) {
      switch ($attribute_name) {
        case 'merlin_id':
          return isset($metadata['cimageid']) ? $metadata['cimageid'] : FALSE;

        case 'caption':
          return isset($metadata['capt2120']) ? $metadata['capt2120'] : FALSE;

        case 'keywords': {
          $keyword_strings = [];

          // Collect all values from keyword fields.
          $fields = [
            'ckey1', 'ckey2', 'ckey3', 'ckey4', 'ckey5', 'ckey6', 'ckey7', 'ckey8', 'ckey255',
            'ckeywords',
          ];
          foreach ($fields as $field) {
            if (!empty($metadata[$field])) {
              foreach (explode(',', $metadata[$field]) as $keyword) {
                $keyword = trim($keyword);
                $keyword_strings[] = $keyword;
              }
            }
          }

          // Make list unique and remove empty values.
          $keyword_strings = array_filter(array_unique($keyword_strings));

          if (count($keyword_strings)) {
            /** @var \Drupal\taxonomy\TermStorageInterface $term_storage */
            $term_storage = $this->getEntityTypeManager()->getStorage('taxonomy_term');
            $keywords = [];

            $handler_settings = $media->getFieldDefinition($field_map[$attribute_name])->getSetting('handler_settings');
            $keywords_vocabulary = count($handler_settings['target_bundles']) > 1 ? $handler_settings['auto_create_bundle'] : reset($handler_settings['target_bundles']);

            foreach ($keyword_strings as $keyword) {
              $term_candidates = $term_storage->loadByProperties([
                'vid' => $keywords_vocabulary,
                'name'  => $keyword,
              ]);

              if (empty($term_candidates) && $handler_settings['auto_create']) {
                $term = $term_storage->create([
                  'vid' => $keywords_vocabulary,
                  'name' => $keyword,
                ]);
                $term->save();
              }
              else {
                $term = array_shift($term_candidates);
              }

              if (isset($term)) {
                $keywords[] = $term->id();
              }
            }

            return count($keywords) ? $keywords : FALSE;
          }
          break;
        }

        default:
          return isset($metadata[$attribute_name]) ? $metadata[$attribute_name] : FALSE;
      }
    }
    elseif (isset($media->{$field_map[$attribute_name]})) {
      return $media->{$field_map[$attribute_name]}->getValue();
    }

    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    $id_message = $this->t('The <em>Merlin ID</em> field must be mapped before importing assets from MerlinOne');

    if ($form_state->get('operation') === 'add' || !$form_state instanceof SubformStateInterface) {
      $form['id_warning'] = [
        '#type' => 'markup',
        '#markup' => '<p class="color-warning">' . $id_message . '</p>',
      ];

      return $form;
    }

    if ($form_state->get('operation') === 'edit') {
      $media_type = $form_state->getCompleteFormState()->get('type');
      $field_map = $media_type->getFieldMap();

      if (empty($field_map[self::MERLIN_ID_SOURCE_FIELD])) {
        $form['id_warning'] = [
          '#type' => 'markup',
          '#markup' => '<p class="color-error">' . $id_message . '</p>',
        ];
      }
    }

    return $form;
  }

  /**
   * Get the entity type manager.
   *
   * @return \Drupal\Core\Entity\EntityTypeManagerInterface
   *   The entity type manager service.
   */
  abstract protected function getEntityTypeManager();

}
