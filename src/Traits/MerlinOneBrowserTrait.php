<?php

namespace Drupal\merlinone\Traits;

use Drupal\media\MediaTypeInterface;
use Drupal\merlinone\Plugin\media\Source\MerlinOneMediaSourceInterface;

/**
 * Helpers for embedding the MerlinOne browser embed.
 */
trait MerlinOneBrowserTrait {

  /**
   * The MerlinOne API Service.
   *
   * @var \Drupal\merlinone\MerlinOneApiInterface
   */
  protected $merlinOneApi;

  /**
   * Gets the MerlinOne API service.
   *
   * @return \Drupal\merlinone\MerlinOneApiInterface
   *   The MerlinOne API service.
   */
  protected function getMerlinOneApi() {
    if (!$this->merlinOneApi) {
      $this->merlinOneApi = \Drupal::service('merlinone.api');
    }

    return $this->merlinOneApi;
  }

  /**
   * Get the Merlin Browser embed.
   *
   * @param int $height
   *   The iframe height.
   * @return array
   *   The browser embed render array.
   */
  protected function getMerlinBrowserEmbed($height) {
    return [
      '#type' => 'html_tag',
      '#tag' => 'iframe',
      '#attributes' => [
        'src' => $this->getMerlinOneApi()->getMxUrl(),
        'class' => 'merlinone-search-iframe',
        'width' => '100%',
        'height' => $height,
        'frameborder' => 0,
        'style' => 'padding: 0; width: 1px !important; min-width: 100% !important; overflow: hidden !important;',
      ],
    ];
  }

  /**
   * Get Merlin browser settings.
   *
   * @param \Drupal\media\MediaTypeInterface $media_type
   *   The media type.
   * @return array
   *   Settings for the Merlin browser.
   */
  protected function getMerlinBrowserSettings(MediaTypeInterface $media_type) {
    $mx_url = $this->getMerlinOneApi()->getMxUrl();

    /** @var MerlinOneMediaSourceInterface $source */
    $source = $media_type->getSource();

    return [
      'mx_host' => parse_url($mx_url)['host'],
      'api_base_url' => $this->merlinOneApi->getApiBaseUrl(),
      'media_type' => $media_type->id(),
      'extensions' => $source->getSupportedExtensions($media_type),
      'should_limit_extensions' => $source->shouldLimitExtensions($media_type),
    ];
  }

}
