/**
 * @file
 *
 * MerlinOne Entity Browser integration
 */
(function ($, Drupal) {

  'use strict';

  Drupal.behaviors.merlinOneEntityBrowserSearch = {
    attach: function (context, settings) {
      var $searchFrame = $(context).find('.merlinone-search-iframe').once('merlinOneEntityBrowserSearch');
      if ($searchFrame.length) {
        // Parent window's MerlinOne processing handler.
        var merlinOneInstance = window.parent.MerlinOne;

        Drupal.merlinOneFormHandler.handleForm(
          context,
          settings,
          merlinOneInstance,
          '.is-entity-browser-submit'
        );
      }
    }
  };

})(jQuery, Drupal);
