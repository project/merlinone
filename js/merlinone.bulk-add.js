/**
 * @file
 *
 * MerlinOne Bulk Add
 */
(function ($, Drupal) {

  'use strict';

  function setIframeHeight() {
    var $frame = $('.merlinone-search-iframe'),
      above = $frame.offset().top,
      below = $('body').outerHeight(true) - above - $frame.height();
    $frame.height(window.innerHeight - above - below - 50);
  }

  Drupal.behaviors.merlinOneBulkAddSearch = {
    attach: function (context, settings) {
      var $searchFrame = $(context).find('.merlinone-search-iframe').once('merlinOneBulkAddSearch');
      if ($searchFrame.length) {
        // Window's MerlinOne processing handler.
        var merlinOneInstance = window.MerlinOne;

        Drupal.merlinOneFormHandler.handleForm(
          context,
          settings,
          merlinOneInstance,
          '.merlinone-bulk-add-form iframe',
          false,
          false
        );
      }

      // Handle iframe sizing
      setIframeHeight();
      window.addEventListener('resize', setIframeHeight);
    }
  };

})(jQuery, Drupal);
