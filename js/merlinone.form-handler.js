/**
 * @file merlinone.form-handler.js
 *
 * Handler for forms using the MerlinOne embedded iframe.
 */

(function ($, Drupal) {

  'use strict';

  function getHost(url) {
    var l = document.createElement('a');
    l.href = url;
    return l.hostname;
  }

  Drupal.merlinOneFormHandler = {};

  Drupal.merlinOneFormHandler.handleForm = function (context, drupalSettings, merlinOneInstance, submitButtonSelector, disableSubmit = true, useGlobalThrobber = true) {
    var merlinSettings = drupalSettings.merlinone,
      mxHost = merlinSettings.mx_host;

    // States
    var isProcessing = false,
      isFinalizing = false;

    // Hold the final list of items to submit.
    var finalItems = [];

    // Hidden input to hold the JSON response from Merlin
    var $merlinMessageResponse = $(context).find('input[name=merlinone_items]');

    // Disable submit button until we get data
    var $submit = $(context).find(submitButtonSelector);
    if (disableSubmit) {
      $submit.prop('disabled', true);
    }

    var $throbber = $(
      '<div class="ajax-progress ajax-progress-throbber">' +
        '<div class="throbber">&nbsp;</div>' +
        '<div class="message">' + Drupal.t('Please wait...') + '</div>' +
      '</div>'
    ).hide();
    $submit.after($throbber);

    var showThrobber = function () {
      $throbber.show();
    };

    var hideThrobber = function () {
      if (!useGlobalThrobber || (!isProcessing && !isFinalizing)) {
        $throbber.hide();
      }
    };

    // Display throbber when button is clicked
    $submit.on('click', function () {
      showThrobber();
    });

    // Items processed callback.
    var itemsProcessed = function (items) {
      finalItems = finalItems.concat(items);
      $merlinMessageResponse.val(JSON.stringify(finalItems));
      $submit.prop('disabled', false);
      isProcessing = false;
      hideThrobber();
    };

    // Use local throbber instead.
    if (!useGlobalThrobber) {
      merlinOneInstance.showThrobber = function () {
        isFinalizing = true;
        showThrobber();
      };

      merlinOneInstance.hideThrobber = function () {
        isFinalizing = false;
        hideThrobber()
      };
    }

    // Receive selected item information.
    var messageHandler =  function (e) {
      if (e.data && getHost(e.origin) === mxHost) {
        var response = JSON.parse(e.data);
        if (response.data && response.message) {
          if (response.message !== 'mxDidShareAssets') return;

          if (response.data.assets) {
            isProcessing = true;
            showThrobber();
            merlinOneInstance.prepareMedia(response.data.assets, response.data.transaction, merlinSettings, itemsProcessed);
          }
          else {
            $merlinMessageResponse.val('');
            $submit.prop('disabled', false);
          }
        }
      }
    };

    // Attach listener
    window.addEventListener('message', messageHandler);
  }


}(jQuery, Drupal));
