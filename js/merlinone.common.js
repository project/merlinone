/**
 * @file merlinone.common.js
 *
 * Media processing commands used by the MerlinOne search widget.
 */

(function ($, Drupal) {

  'use strict';

  function MerlinOne() {
    this.currentBackgroundItems = [];
    this.$throbber = $(
      '<div class="merlinone--processing">' +
        '<div class="ajax-progress ajax-progress-throbber">' +
          '<div class="throbber">&nbsp;</div>' +
          '<div class="message">' + Drupal.t('Processing media, please wait...') + '</div>' +
        '</div>' +
      '</div>'
    ).hide();

    this.showThrobber = function () {
      this.$throbber.show();
    };
    this.hideThrobber = function () {
      this.$throbber.hide();
    };

    $(document.body).append(this.$throbber);
  }

  /**
   * Prepare Media entities by precreating them using the thumbnail, or if a
   * non-image type, loading the full URL.
   *
   * @param {[]} items
   * @param {[]} transaction
   * @param {{}} settings
   * @param {function([])} callback
   */
  MerlinOne.prototype.prepareMedia = function(items, transaction, settings, callback) {
    if (!items.length)
      return;

    var processedItems = [],
      doneCount = 0;

    var processed = function(item) {
      processedItems.push(item);
    }

    var self = this;
    var alwaysHandler = function () {
      doneCount++;
      if (doneCount === items.length) {
        // Pass prepared items back to caller.
        callback(processedItems);

        // Finalize items.
        self.finalizeMedia(processedItems, transaction, settings);
      }
    };

    // Prepare items and pass them to the callback.
    for (var i = 0; i < items.length; i++) {
      this.prepareMediaItem(items[i], transaction, settings, processed)
        .fail(this._errorHandler)
        .always(alwaysHandler);
    }
  }

  /**
   * Prepare a single Media item.
   *
   * @param {{}} item
   * @param {{}} transaction
   * @param {{}} settings
   * @param {function({})} callback
   * @return {jQuery.Deferred}
   */
  MerlinOne.prototype.prepareMediaItem = function(item, transaction, settings, callback) {
    var deferred = $.Deferred();

    var mediaType = settings.media_type;

    /**
     * Create initial media entities and set their returned entity ID.
     *
     * @param {{}} item
     */
    var doPreparePost = function(item) {
      $.ajax({
        method: 'POST',
        url: '/merlinone/prepare/' + mediaType,
        data: { item, transaction, settings },
        dataType: 'json',
        cache: false,
        success: function (result) {
          callback(result);
          deferred.resolve(result);
        },
        error: function (xhr, status, message) {
          deferred.reject(message, status);
        }
      });
    };

    // Prepare the item.
    doPreparePost(item);

    return deferred.promise();
  }

  /**
   * Track finalization progress.
   *
   * @param {{}} item
   */
  MerlinOne.prototype.onFinalizeProgress = function(item) {
    this.currentBackgroundItems = this.currentBackgroundItems.filter(function (i) {
      return i !== item;
    });

    if (!this.currentBackgroundItems.length) {
      this.hideThrobber();

      // Show success message.
      const message = new Drupal.Message();
      const messageId = message.add(
        Drupal.t('Media added'),
        { type: 'status' }
      );

      // Remove success message after a timeout.
      setTimeout(function () {
        message.remove(messageId);
      }, 5000);
    }
  }

  /**
   * Finalize Media entities by sending the fetched full asset URL.
   *
   * @param {{}} items
   * @param {{}} transaction
   * @param {{}} settings
   */
  MerlinOne.prototype.finalizeMedia = function(items, transaction, settings) {
    if (!items.length)
      return;

    // Add current items to the tracked items.
    this.currentBackgroundItems = this.currentBackgroundItems.concat(items);
    if (this.currentBackgroundItems.length) {
      // Show the throbber.
      this.showThrobber();
    }

    var self = this;
    var failHandler = function (item, message, status) {
      self.onFinalizeProgress(item);
      self._errorHandler(message, status);
    }

    var alwaysHandler = function (item) {
      self.onFinalizeProgress(item);
    }

    // Finalize items and pass them to the callback.
    for (var i = 0; i < items.length; i++) {
      this.finalizeMediaItem(items[i], transaction, settings)
        .fail(failHandler)
        .always(alwaysHandler)
    }
  }

  /**
   * Finalize a single item.
   *
   * @param {{}} item
   * @param {{}} transaction
   * @param {{}} settings
   * @return {jQuery.Deferred}
   */
  MerlinOne.prototype.finalizeMediaItem = function(item, transaction, settings) {
    var deferred = $.Deferred();

    var mediaType = settings.media_type,
      apiBaseUrl = settings.api_base_url;

    // Force JPEG for types unsupported by the configured source image field and toolkit.
    if (settings.should_limit_extensions && transaction && item.metadata && item.metadata['hires']) {
      var hires = item.metadata['hires'],
        ext = hires.substr(hires.lastIndexOf('.') + 1).toLowerCase(),
        accepted = settings.extensions;
      if (ext && !accepted.includes(ext)) {
        transaction['image.format'] = 'jpg';
      }
    }

    /**
     * Send asset containing the full asset URL.
     *
     * @param {{}} item
     */
    var doFinalizePost = function(item) {
      $.ajax({
        method: 'POST',
        url: '/merlinone/finalize/' + mediaType + '/' + item.entityId,
        data: { item, transaction, settings },
        dataType: 'json',
        cache: false,
        success: function () {
          deferred.resolve(item);
        },
        error: function (xhr, status, message) {
          deferred.reject(item, message, status);
        }
      });
    };

    // Get the full URL and finalize.
    this.getFullAssetUrl(item, transaction, apiBaseUrl, doFinalizePost);

    return deferred.promise();
  }

  /**
   * Get an asset's full URL.
   *
   * @param {{}} item
   * @param {{}} transaction
   * @param {string} baseApiUrl
   * @param {function} callback
   * @return {function}
   */
  MerlinOne.prototype.getFullAssetUrl = function (item, transaction, baseApiUrl, callback) {
    var deferred = $.Deferred();

    var data = {};
    for (var key in transaction) {
      var val = transaction[key];
      if (val !== null && !(typeof val === 'string' && val.length === 0)) {
        data[key] = val;
      }
    }

    $.ajax({
      method: 'POST',
      url: baseApiUrl + 'asset/' + item.identifier + '/url',
      data: data,
      dataType: 'json',
      cache: false,
      contentType: 'application/x-www-form-urlencoded; charset=UTF-8',
      xhrFields: {
        withCredentials: true
      },
      success: function (result) {
        item.fullAssetUrl = result;
        callback(item);
        deferred.resolve(result);
      },
      error: function (xhr, status, message) {
        deferred.reject(message, status);
      }
    });

    return deferred.promise();
  }

  /**
   * Error handler.
   *
   * @param {string} message
   * @param {{}} status
   * @private
   */
  MerlinOne.prototype._errorHandler = function (message, status) {
    console.log('MX error: ' + message);
    console.log(status);

    alert('An error occurred.\n\n' + message);
  }

  // Initialize an instance on the window object if it doesn't exist.
  window.MerlinOne = window.MerlinOne || new MerlinOne();

}(jQuery, Drupal));
